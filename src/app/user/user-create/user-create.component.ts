import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/template/header/header.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  user: User = {
    email: '',
    first_name: '',
    last_name: '',
    avatar: ''
  };

  constructor(
    private userService: UserService,
    private router: Router,
    headerService: HeaderService
  ) {
    headerService.headerData = {
      title: 'Novo Usuário',
      icon: 'add',
      routeUrl: '/users/create'
    };
   }

  ngOnInit() {
  }

  createUser(): void {
    this.userService.create(this.user).subscribe(() => {
      this.userService.showMessage('Usuário criado!');
      this.router.navigate(['/users']);
    });
  }

  cancel(): void {
    this.router.navigate(['/users']);
  }

}
