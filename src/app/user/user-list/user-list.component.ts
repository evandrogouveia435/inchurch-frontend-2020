import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];
  totalPages;
  pageNumbers: any;
  currentPage;
  displayedColumns = ['avatar', 'first_name', 'last_name', 'email', 'action'];

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.getUsersFor();
  }

  getUsersFor(pageNumber = 1) {
    this.userService.listUsersForPage(pageNumber)
      .subscribe((response: any) => {
        this.users = response.data;
        this.totalPages = response.total_pages;
        this.pageNumbers = new Array(this.totalPages).fill(pageNumber).map((item, index) => index + 1);
        this.currentPage = response.page;
        this.router.navigate(['/users']);
      }, (error) => {
        console.log('Got the Error as:', error);
      });
  }

}
