import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { User } from './user.model';
import { Observable, EMPTY } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = 'https://reqres.in/api/users';

  constructor(
    private snackBar: MatSnackBar,
    private http: HttpClient
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // show messages
  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top",
      panelClass: isError ? ['msg-success'] : ['msg-error']
    });
  }

   // list users per page
  listUsersForPage(pageNumber): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}?page=${pageNumber}`)
      .pipe(
        map((obj) => obj),
        catchError(e => this.errorHandler(e))
      );

  }

  // user details
  getUserDetails(userId) {
    return this.http
      .get<User>(`${this.baseUrl}/${userId}`)
      .pipe(
        map((response: any) => response.data)
      );
  }

  // list users by ID
  listById(id: number): Observable<User> {
    const url = `${this.baseUrl}/${id}`
    return this.http.get<User>(url)
      .pipe(
        map((obj) => obj),
        catchError(e => this.errorHandler(e))
      );
  }

  // create user
  create(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user)
      .pipe(
        map((obj) => obj),
        catchError(e => this.errorHandler(e))
      );
  }

  // update user
  update(user: User): Observable<User> {
    const url = `${this.baseUrl}/${user.id}`
    return this.http.put<User>(url, user)
      .pipe(
        map((obj) => obj),
        catchError(e => this.errorHandler(e))
      );
  }

  // delete user
  delete(id: number): Observable<User> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<User>(url)
      .pipe(
        map((obj) => obj),
        catchError(e => this.errorHandler(e))
      );
  }

  // error message
  errorHandler(e: any): Observable<any> {
    this.showMessage('Ocorreu um erro!', true);
    return EMPTY;
  }
}
